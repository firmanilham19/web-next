import Link from 'next/link'
import styles from './Header.module.css'

const Header = () => {
    return (
        <>
            <header className={styles.container}>
                <ul className={styles.list}>
                    <li className={styles.item}><Link href="/">Home</Link></li>
                    <li className={styles.item}><Link href="/services">Services</Link></li>
                    <li className={styles.item}><Link href="/clients">Clients</Link></li>
                    <li className={styles.item}><Link href="/users">Users</Link></li>
                    <li className={styles.item}><Link href="/blog">Blogs</Link></li>
                </ul>
            </header>
        </>
    )
}

export default Header