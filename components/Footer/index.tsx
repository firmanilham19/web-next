import styles from './Footer.module.css'

const Footer = () => {
    return (
        <>
            <footer className={styles.footer}>
                Copyright Koneksi Group
            </footer>
        </>
    )
}

export default Footer