import Layout from "../../components/Layout";
import Head from 'next/head';

export default function Client() {
    return (
        <>
        <Layout pageTitle="Our Client">
            <div>
                <h1>Our Clients</h1>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Reiciendis, sapiente. Eos sint voluptatum et fugit!</p>
            </div>
        </Layout>
        </>
    )
}