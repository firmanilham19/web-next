import Layout from "../../components/Layout";
import { useRouter } from "next/router";

export default function Client() {
    const router = useRouter();
    const { slug } = router.query;

    return (
        <>
            <Layout pageTitle="Our Client">
                <div>
                    <h1>Our Clients</h1>
                    <h2>{slug}</h2>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Reiciendis, sapiente. Eos sint voluptatum et fugit!</p>
                </div>
            </Layout>
        </>
    )
}