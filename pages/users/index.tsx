import Layout from "../../components/Layout";
import Link from 'next/link'

interface UserProps {
    users: Array<string>;
}

export default function Users(props: UserProps) {
    const { users } = props

    return (
        <>
            <Layout pageTitle="Users">
                <div>
                    <h1>Our Users</h1>
                    <ul>
                        {users.map((item: any, i: number) => {
                            return (
                                <li key={i}>
                                    <Link href={`/users/${item.id}`}>
                                        {item.name}
                                    </Link>

                                </li>
                            )
                        })}
                    </ul>
                </div>
            </Layout>
        </>
    )
}

// getStaticProps is best for non-dynamic data
// the data that does not change frequently
export async function getStaticProps() {
    const response = await fetch('https://jsonplaceholder.typicode.com/users')
    const users = await response.json()

    return {
        props: {
            users
        }
    }

}