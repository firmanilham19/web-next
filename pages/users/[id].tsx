import Layout from "../../components/Layout";
import { useRouter } from "next/router";

interface User {
    id: number,
    name: string,
    email: string,
    phone: string
}

interface UserDetailProps {
    user: User;
}

export default function Users(props: UserDetailProps) {
    const router = useRouter();
    const { id } = router.query;

    const { user } = props;

    return (
        <>
            <Layout pageTitle="User Detail">
                <div>
                    <h1>User Page</h1>
                    <h2>User ID {id}</h2>
                    <p>{user.name}</p>
                    <p>{user.email}</p>
                    <p>{user.phone}</p>
                </div>
            </Layout>
        </>
    )
}

export async function getStaticPaths(params: any) {
    const response = await fetch('https://jsonplaceholder.typicode.com/users')
    const users = await response.json()

    const paths = users.map((item: User) => {
        return {
            params: {
                id: item.id + '',
            }
        }
    })

    return {
        paths,
        fallback: false
    }
}

interface GetStaticProps {
    params: {
        id: string
    }
}

export async function getStaticProps(context: GetStaticProps) {
    const { id } = context.params
    const response = await fetch('https://jsonplaceholder.typicode.com/users/' + id)
    const user = await response.json()

    return {
        props: {
            user
        }
    }
}