import { useEffect } from "react"
import { useRouter } from "next/router"

export default function NotFound() {
    const router = useRouter();

    useEffect(() => {
        setTimeout(() => {
            router.push("/")
        }, 2000)
    }, [])

    return (
        <>
            <div> {`Seems you're getting lost?`}</div>
        </>
    )
}