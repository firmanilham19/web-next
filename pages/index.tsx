import type { NextPage } from 'next'
import Head from 'next/head'
import Image from 'next/image'
import Link from 'next/link'
import styles from '../styles/Home.module.css'
import Layout from '../components/Layout'

const Home: NextPage = () => {
  return (
    <>
      <Layout pageTitle='Homepage'>
        <h1 className='text-red'>Hello World!</h1>
        <p className={styles.hello}>Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloremque, quibusdam.</p>
      </Layout>
    </>
  )
}

export default Home
