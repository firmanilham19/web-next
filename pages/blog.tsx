import Layout from "../components/Layout"

interface Post{
    id: number,
    title: string,
    body: string
}

interface BlogProps{
    dataBlog: Array<Post>
}

export default function Blog(props: BlogProps) {
    const {dataBlog} = props;

    return (
        <Layout pageTitle="Blog Page">
            <div>
                <h1>Blog Page</h1>
                {dataBlog.map((item) => {
                    return(
                        <div key={item.id} style={{border:'1px solid black', marginBottom:'8px'}}>
                            <h3>{item.title}</h3>
                            <p>{item.body}</p>
                        </div>
                    )
                })}
            </div>
        </Layout>
    )
}

export async function getServerSideProps() {
    const response = await fetch(`https://jsonplaceholder.typicode.com/posts`);
    const dataBlog = await response.json();
    return {
        props: {
            dataBlog
        }
    }
}